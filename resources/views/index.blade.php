<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Codeinhouse Vue Laravel App</title>
    <link rel="stylesheet" href="{{url('css')}}/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('css')}}/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{url('css')}}/font-awesome.min.css">
    <link rel="stylesheet" href="/css/app.css">

</head>
<body>

<div id="app"></div>


<script src="{{url('js')}}/jquery-3.2.1.min.js"></script>
<script src="{{url('js')}}/bootstrap.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>