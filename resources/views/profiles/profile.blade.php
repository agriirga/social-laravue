@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-center">
                        {{ $user->name}} 's Profile
                    </p>
                </div>
                <div class="panel-body">
                    <center>
                        <img src="{{ Storage::url($user->avatar)}}" width="70px" height="70px" style="border-radius: 50%;" alt="">
                    </center>
                </div>

                <p class="text-center">
                            {{ $user->profile->location }}
                </p>

                <p class="text-center">
                    @if(Auth::id() == $user->id)
                        <a href="{{route ('profile.edit')}}" class="btn btn-md btn-info"> Edit Your Profile </a>
                    @endif
                </p>
            </div>
        </div>
    </div>
    
    @if(Auth::id() !== $user->id)
        <div class="row">
            <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Place For Component
                        </div>

                        <div class="app">
                            <div class="panel-body">
                                <friend :profile_user_id ="{{$user->id}}"> </friend>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    @endif



    <div class="row"> 
        <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        About me
                    </div>

                    <div class="panel-body">
                        <p class="text-center">
                            {{ $user->profile->about }}
                        </p>

                        
                    </div>
                </div>
        </div>         
    </div>

    </div>
    @endsection