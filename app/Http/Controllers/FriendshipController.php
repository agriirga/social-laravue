<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class FriendshipController extends Controller
{
    public function check($id){
        if(Auth::user()->isFriendWith($id))
            return ['friendship_status' => 'friends'];

        if(Auth::user()->hasPendingFriendRequestFrom($id))
            return ['friendship_status' => 'pending'];

        if(Auth::user()-> hasPendingFriendRequestSentTo($id))
            return ['friendship_status'=> 'waiting'];


        return ['friendship_status' => 0];
    }

    public function addFriend($id){
        // sending notif
        return Auth::user()->addFriend($id);
    }

    public function acceptFriend($id){
        // sending notif
        return Auth::user()->acceptFriend($id);
    }


}
