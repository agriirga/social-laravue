<?php

namespace App\Traits;
use App\Friendship;
use Illuminate\Support\Facades\Auth;
use App\User;

trait Friendable{

    public function hello(){

        return 'Hello friends';
    }
    

    public function addFriend($friend_id){

        if($this->id === $friend_id)
            return 0;

        if($this->hasPendingFriendRequestSentTo($friend_id) === 1)
            return 'already sent request';

        if($this->hasPendingFriendRequestFrom($friend_id) === 1)
            return $this->acceptFriend($friend_id);


        if($this->isFriendWith($friend_id) === 1)
            return "already friend";

        $friendship  = Friendship::create([
            'user_id' => $this->id,
            'friend_id' => $friend_id
        ]);

        if($friendship){
            return response()->json($friendship,200);           
        } 

        return response()->json('fail',501);           
    }

    public function acceptFriend($requester_id){

        if($this->hasPendingFriendRequestFrom($requester_id) === 0)
            return 0;


        $friendship = Friendship::where('user_id',$requester_id)
            ->where('friend_id', $this->id)->first();

        if($friendship){
            $friendship->update([
                'status' => 1
            ]);
   
            return response()->json($friendship,200);
   
        }

        return response()->json('fail',501);
    }

    public function friends(){
        $friend_id = array();

        $my_request = Friendship::select('friend_id')
                    ->where('status',1)
                    ->where('user_id', Auth::id())->get();

        foreach($my_request as $friend){
            array_push($friend_id, $friend->friend_id);
        }

        $friend_request = Friendship::select('user_id')
                ->where('status',1)
                ->where('friend_id', Auth::id())->get();

        foreach($friend_request as $friend){
            array_push($friend_id, $friend->user_id);
        }

        $friends = User::whereIn('id', $friend_id )
                    ->where('id','<>', Auth::id())->get();
        
        // dd($friends->pluck('name'));
        return $friends;        
    }

    public function friends_ids(){
        return collect($this->friends())->pluck('id')->toArray();

    }

    public function isFriendWith($friend_id){
        
        if(in_array($friend_id,$this->friends_ids()))
            return 1;
        else    
            return 0;
    }


    public function pendingReceiveFriendRequest(){

        $friendships  = Friendship::select('user_id')->where('status',0)
            ->where('friend_id', $this->id)->get();
        // dd($friendships);
        $users = User::whereIn('id',$friendships->pluck('user_id'))->get();

        // dd($users);
        return $users;
    }

    public function  pendingSentFriendRequest(){


        $friendships  = Friendship::select('friend_id')
            ->where('status',0)
            ->where('user_id', $this->id)->get();


        // dd($friendships);
        $users = User::whereIn('id',$friendships
            ->pluck('friend_id'))->get();

        return $users;
    }

    public function pendingSentFriendRequestId()
    {
        return collect($this->
            pendingSentFriendRequest()
            )->pluck('id')->toArray();
    }

    public function pendingReceiveFriendRequestId()
    {
        $arr = collect($this->pendingReceiveFriendRequest())
            ->pluck('id')->toArray();
        
        // dd($arr);
        return $arr;
    }

    public function hasPendingFriendRequestFrom($user_id){
        $request_from_id = $this->pendingReceiveFriendRequestId();
        // dd($request_from_id);
        if(in_array($user_id , $request_from_id))
            return 1;
        else
            return 0;
    }

    public function hasPendingFriendRequestSentTo($user_id){
        $request_to = $this->pendingSentFriendRequestId();
        if(in_array($user_id , $request_to ))
            return 1;
        else
            return 0;
    }




}