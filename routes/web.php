<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/check_relationship_status/{id}', 
//     function ($id) {
//         return \App\User::find($id);
// });

Route::get('/check_relationship_status/{id}', 
[
    'uses'  => 'FriendshipController@check',
    'as' => 'check'  
]);

Route::get('/add_friend/{id}', 
[
    'uses'  => 'FriendshipController@addFriend',
    'as' => 'add_friend'  
]);

Route::get('/accept_friend/{id}', 
[
    'uses'  => 'FriendshipController@acceptFriend',
    'as' => 'accept_friend'  
]);

Route::get('/home', 'HomeController@index')->name('home');


/*
Route::get('/add_friend', function(){
    return Auth::user()->addFriend(5);  
});

Route::get('/accept_friend', function(){
    return \App\User::find(8)->acceptFriend(Auth::id());  
});

Route::get('/friends', function(){
    return Auth::user()->friends(Auth::id());  
});

Route::get('/friends_ids', function(){
    return Auth::user()->friends_ids();  
});

Route::get('/sent_pending', function(){
    return Auth::user()->pendingSentFriendRequest();
});

Route::get('/sent_pending_id', function(){
    return Auth::user()->pendingSentFriendRequestId();
});

Route::get('/receive_pending', function(){
    return Auth::user()->pendingReceiveFriendRequest();
});

Route::get('/receive_pending_id', function(){
    return Auth::user()->pendingReceiveFriendRequestId();
});

Route::get('/has_request_pending_from', function(){
    return Auth::user()->hasPendingFriendRequestFrom(9);
});

Route::get('/has_request_pending_to', function(){
    return Auth::user()->hasPendingFriendRequestSentTo(15);
});

Route::get('/is_friend_with', function(){
    return Auth::user()->isFriendWith(10);
});
*/

Route::group(['middleware' => 'auth'], function(){
    Route::get('profile/{slug}', [
        'uses' => 'ProfileController@index',
        'as' => 'profile'
     ]);

    Route::get('profile/edit/profile', [
        'uses' => 'ProfileController@edit',
        'as' => 'profile.edit'
    ]);

    Route::post('profile/update/profile', [
        'uses' => 'ProfileController@update',
        'as' => 'profile.update'
    ]);
});