<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/*
    $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('slug');
            $table->string('avatar'); 
            $table->string('password');
            $table->boolean('gender');

*/

$factory->define(App\User::class, function (Faker $faker) {
    $name = $faker->name;

    return [
        'name' => $name,
        'email' => $faker->unique()->safeEmail,
        'slug' => str_slug($name),
        'avatar' => 'public/defaults/avatar/male.png',   
        'gender' => '0',
        'password' => bcrypt('secret'), // secret
        'remember_token' => str_random(10),
    ];
});
